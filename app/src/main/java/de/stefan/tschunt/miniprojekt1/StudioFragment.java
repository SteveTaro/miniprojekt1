package de.stefan.tschunt.miniprojekt1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class StudioFragment extends Fragment implements View.OnClickListener {

    View view;
    RelativeLayout newsItem1;
    Button buttonGetToCourse;
    Button buttonGetToStudio;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_studio, container, false);

        newsItem1 = (RelativeLayout) view.findViewById(R.id.newsItem1);
        newsItem1.setOnClickListener(this);

        buttonGetToCourse = (Button) view.findViewById(R.id.buttonGetToCourse);
        buttonGetToCourse.setOnClickListener(this);

        buttonGetToStudio = (Button) view.findViewById(R.id.buttonGetToStudio);
        buttonGetToStudio.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == newsItem1){
            startActivity(new Intent(getContext(), SearchStudioActivity.class));
        }else if(v == buttonGetToCourse){
            startActivity(new Intent(getContext(), StudioCoursesActivity.class));
        }else if(v == buttonGetToStudio){
            Toast.makeText(getContext(), "Öffne Maps", Toast.LENGTH_LONG).show();
        }
    }
}