package de.stefan.tschunt.miniprojekt1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class PersonalRecordFragment extends Fragment implements View.OnClickListener {

    RelativeLayout buttonTrainingAdd;
    RelativeLayout buttonTrainingEdit;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_personal_record, container, false);

        buttonTrainingAdd = (RelativeLayout) view.findViewById(R.id.buttonTrainingAdd);
        buttonTrainingEdit = (RelativeLayout) view.findViewById(R.id.buttonTrainingEdit);

        buttonTrainingAdd.setOnClickListener(this);
        buttonTrainingEdit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == buttonTrainingAdd){
            Toast.makeText(getContext(), "Training hinzufügen", Toast.LENGTH_LONG).show();
        }else if(v == buttonTrainingEdit){
            Toast.makeText(getContext(), "Training anpassen", Toast.LENGTH_LONG).show();
        }
    }
}