package de.stefan.tschunt.miniprojekt1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class FeedFragment extends Fragment implements View.OnClickListener {

    View view;

    private Button buttonFeed;
    private Button buttonFeed1;
    private Button buttonFeed2;
    private Button buttonFeed3;
    private Button buttonFeed4;
    private Button buttonFeed5;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feed, container, false);

        buttonFeed = (Button) view.findViewById(R.id.buttonFeed);
        buttonFeed1 = (Button) view.findViewById(R.id.buttonFeed1);
        buttonFeed2 = (Button) view.findViewById(R.id.buttonFeed2);
        buttonFeed3 = (Button) view.findViewById(R.id.buttonFeed3);
        buttonFeed4 = (Button) view.findViewById(R.id.buttonFeed4);
        buttonFeed5 = (Button) view.findViewById(R.id.buttonFeed5);

        buttonFeed.setOnClickListener(this);
        buttonFeed1.setOnClickListener(this);
        buttonFeed2.setOnClickListener(this);
        buttonFeed3.setOnClickListener(this);
        buttonFeed4.setOnClickListener(this);
        buttonFeed5.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == buttonFeed
                || v == buttonFeed1
                || v == buttonFeed2
                || v == buttonFeed3
                || v == buttonFeed4
                || v == buttonFeed5){
            startActivity(new Intent(getContext(), FeedDetailActivity.class));
        }
    }
}